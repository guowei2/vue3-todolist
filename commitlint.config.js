/** @type {import('cz-git').UserConfig} */
export default {
  prompt: {
    useEmoji: true
  },
  extends: ['@commitlint/config-conventional']
}
